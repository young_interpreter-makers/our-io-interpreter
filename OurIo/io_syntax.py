# Io interpreter v1.0.0-alpha
# 7103
# Golofaev M A
# Rykhlov V V
# Shtepen A V
# 21 dec 2020

from time import sleep
import os

try: 
    os.remove('_done')
except:
    pass

try:
    _fname = open('_filename', 'w')
    filename=input()
    _fname.write(filename)
    _fname.close()
except:
    print("Couldn`t make file with "+filename)
    sleep(2)
    exit()
    

try:
    os.startfile('_x64')
except:
    os.startfile('_x86')

while(True):
    try:
        _donefile = open('_done','r')
        _donefile.close()
        break
    except:
        pass 


_signs=[',', ';', ':=', '<', '>', '<=', '>=', '!=',  '==' , ')', '(', '=', ':', ', ', '/', '%', '*', '-', '+']  # add new as first element!!!!

# operators: 
_operators=['.','~', '!', '@', '$', '^', '&', '=', '{', '}', '[', ']', '|', '<', '>', '?', ':', '*', '%', '/', '+', '-', ':=', '::='] 
# signs that included in grammar:
_signs_in_grammar=['(', ',', ')']
# terminators:
_T = [';', '\n', '\r']
# separators:
_Sep = [' ', '\f', '\t', '\v']
# sctpad
sctpad = _T.copy() + _Sep.copy()

# Reserved commands
# if word is reserved then its number in the Lexeme-Flow is -len-0.5+k where k is its index and len is len(reserved)
# (add new in the end of this list)
_reserved=['print', 'for', 'return', 'continue', 'break', 'println',
           'if', 'ifTrue', 'ifFalse', 'ifNil', 'ifNonNil', 'then', 
           'else', 'loop', 'repeat', 'clone', 'foreach', 'map', 'select', 'size', 
           'slice', 'split', 'asNumber', 'File', 'Directory', 'Date'] # add as last !!!!!!!!!



#filename = "prog.txt" #input()
#lib_lex.scan(ctypes.c_char_p(filename.encode('utf-8')))


def _error(error_number, line_number): # function that prints errno and closes the interpreter
    print("Syntax error ("+str(error_number)+") in line "+str(line_number)+". See docs for more info.")
    sleep(1)
    exit()

class io_string(object): # io string with "print" method
    def __init__(self, value, valtype='c'):
        val = str(value)
        self.value = val
        self.valtype=valtype # 'c' if const, 'v' if variable
    def print(self):
        print(self.value)
    def println(self): # print from new line
        print('\n')
        print(self.value)

class io_number(object): # io number with print + - / // % methods
    def __init__(self, value, valtype='c'):
        self.value = value
        self.valtype=valtype # 'c' if const, 'v' if variable
    def __add__(self, other):
        return io_number(self.value + other.value, 'v') # +
    def __sub__(self, other):
        return io_number(self.value - other.value, 'v') # -
    def __mul__(self, other):
        return io_number(self.value * other.value, 'v') # *
    def __truediv__(self, other):
        return io_number(self.value / other.value, 'v') # /
    def __floordiv__(self, other):
        return io_number(self.value // other.value, 'v') # // 
    def __mod__(self, other):
        return io_number(self.value % other.value, 'v') # %
    def print(self):
        print(self.value) # print method 
    def println(self):
        print('\n')
        print(self.value)    

class io_unknown_variable(object): # 
    def __init__(self):
        self.value=''
    def convert(self, value):
        if type(value)==type(""): # if value is a string
            self = io_string(value, 'v')
    
lexemes_file = open("lexemes.txt") # open file of lexemes
table_file = open("table.txt") # open file with variables table
lexemes = lexemes_file.read().split('\n') # read the whole lexemes file and split it into lines
_table = table_file.read().split('\n') # --//--
lexemes_file.close() # close files
table_file.close() # --//--

for k in range(len(lexemes)): # create list of [list that contatins all lexemes on the line] --
    lexemes[k] = lexemes[k].split() # -- so its first index is number of the line ---
    for j in range(len(lexemes[k])): # --- and the second is number of the lexeme(token) on the line
        lexemes[k][j] = int(lexemes[k][j])

k=0
while k<len(lexemes): # delete empty lines from the lexemes list
    if len(lexemes[k])==0:
        lexemes.pop(k)
        k-=1
    k+=1

k=0
while k<len(_table): # delete empty lines from the table lines list
    if len(_table[k])==0:
        _table.pop(k)
        k-=1
    k+=1
    
table = []
table_info_len=4
for k in range(len(_table)): # transformation table of (string of variable description) --
    _var_line = _table[k].split(' ') # -- into table of (list with real variables)
    _var_line[0]=int(_var_line[0])
    if _var_line[1]=='str':
        newstring = ''
        len_var_line=len(_var_line)
        for s in range(3, len_var_line): 
            if s>3: newstring+=' '
            newstring+=_var_line[s]
        while len(_var_line)-table_info_len>0: # if it is a line
            _var_line.pop()
        _var_line[3]=io_string(newstring)
        table.append(_var_line)
    elif (_var_line[1]=='int') or (_var_line[1]=='float') or (_var_line[1]=='number'): # if it is a number
        _var_line[3]=_var_line[3].replace(',', '.')
        _var_line[3]=float(_var_line[3])
        if int(_var_line[3])==_var_line[3]:
            _var_line[3]=io_number(int(_var_line[3]))
        else:
            _var_line[3]=io_number(_var_line[3])
        table.append(_var_line)
    elif (_var_line[2]=='sign'): # if it is a sign
        _var_line[3]=_signs.index(_var_line[3])-len(_signs)
        table.append(_var_line)
    elif (_var_line[2]=='id'): # if it is id-like
        if _var_line[3] in _reserved: # if it is a key word
            _var_line[1]='reserved'
            _var_line[3]=_reserved.index(_var_line[3])-len(_reserved)-0.5
            table.append(_var_line)
        else:
            table.append(_var_line) # if it is ID of a variable



for k in range(len(lexemes)): # writes numbers of reserved words and signs into lexemes flow 
    for j in range(len(lexemes[k])):
        if table[lexemes[k][j]][1] =='reserved':
            lexemes[k][j] = table[lexemes[k][j]][3]
        elif table[lexemes[k][j]][2] =='sign':# and _signs[table[lexemes[k][j]][3]] in [':']:
            #if table
            lexemes[k][j] = table[lexemes[k][j]][3]
    _LEN=len(lexemes[k])
    j=0
    while j < _LEN: # if there is lexemes : and = then replace it with :=
        if (lexemes[k][j]==int(lexemes[k][j])): 
            if lexemes[k][j]<0:
                if _signs[lexemes[k][j]] in [':']:
                    if (j<(len(lexemes[k])-1)) and _signs[lexemes[k][j+1]] in ['=']:
                        lexemes[k].pop(j+1)
                        lexemes[k][j]=_signs.index(':=')-len(_signs)
                        _LEN-=1
                        if j>0 and j<_LEN:
                            if table[lexemes[k][j-1]][1] in ['unknown'] and table[lexemes[k][j-1]][2] in ['id']: 
                                if type(table[lexemes[k][j+1]][3]) == type(io_string("")):
                                    table[lexemes[k][j-1]][3] = io_string("", 'v')
                                    table[lexemes[k][j-1]][1] = 'strv' # if x = str then x is str-variable
                                elif type(table[lexemes[k][j+1]][3]) == type(io_number("")):
                                    table[lexemes[k][j-1]][3] = io_number("", 'v')
                                    table[lexemes[k][j-1]][1] = 'numberv' # if x = number then x is number-variable
                        continue        
        j+=1   
                
                

############# Syntax Analysis
checkbox = [-1 for k in range(len(lexemes))] # -1 if this line hasn`t been checked yet; --
# --0 if syntax error;  if OK

stack = []  
omega = []

def shift():
    global omega
    global stack
    stack.append(omega.pop(0))


openbrac = 0
for k in range(len(lexemes)):
    omega = lexemes[k].copy()
    stack=[]
    for j in range(len(omega)):
        if omega[j] >= 0: # if it`s a variable or string or number
            x=table[omega[j]][1]
            if x =='str':
                omega[j] = 'Str'
            elif x in ['int', 'float', 'number']:
                omega[j]='N'
            else:
                omega[j]='I'
        elif omega[j]<0 and omega[j]==int(omega[j]): # if it`s a key from _signs
            if _signs[omega[j]+len(_signs)] in _signs_in_grammar: # if it is '(' or ')' or ','
                omega[j] = _signs[omega[j]+len(_signs)]
            elif _signs[omega[j]+len(_signs)] in _operators:
                omega[j] = 'OP'
            elif _signs[omega[j]+len(_signs)] in sctpad:
                omega[j] = "SCTPAD"
        else:
            omega[j]='I'
    print('line', k)
    flag=0
    while(True):
        #if k==0:
        print("w =", omega,"stack =", stack)
        # cells below are described in Operator_grammar_table.docx
        if len(stack)==0: # if stack is empty then shift
            shift()
            continue
        if (stack[-1] in ['ME']): # ME
            if len(omega)>0 and omega[0] in [')']: # cell 0
                stack[-1]='Z'
                continue
            if len(omega)<1: 
                stack[-1]='Z'
                continue
            if len(omega)>0 and omega[0] in _signs_in_grammar: # cell 2
                if omega[0] in [',']:
                    stack[-1]='Z'
                    continue
                _error(1, k) 
            else:
                if len(omega)>0: shift() # cell 1
                continue
        if stack[-1] in ['E']:
            #if len(omega)>0 and omega[0] in _signs_in_grammar:
             #   _error(2, k)
            if not ((len(stack)>=2) and (stack[-2] in ['ME'])): # cells 3, 4
                stack[-1]='ME'
                continue
        if (len(stack)>=2) and (stack[-2] in ['ME']) and (stack[-1] in ['E']): # cell 5
            stack.pop()
            continue
        if stack[-1] in ['M', 'SCTPAD']: # cell 6
            stack[-1]='E'           
            continue
        if stack[-1] in ['S']:
            if len(omega)>0 and (omega[0] in ['(', 'N']): # cell 8 and additional_rule_1: shifting N after S
                shift()
                continue
            #elif len(omega)>0 and omega[0] in _signs_in_grammar:
            #    _error(3, k)
            else: # cells 7, 9
                stack[-1]='M'
                continue
        if (len(stack)>=2) and (stack[-2] in ['S']) and (stack[-1] in ['As']): 
            if len(omega)>0 and omega[0] in ['(']: # cell 11
                _error(4, k)
            elif len(omega)>0 and omega[0] in _signs_in_grammar: # cell 12
                _error(5, k)
            else: # cell 10
                stack.pop() 
                stack[-1]='M'
                continue        
        if (len(stack)>=2) and (stack[-2] in ['(']) and (stack[-1] in ['As']) and len(omega)>0 and omega[0] in [')']:
            stack.pop(-2) # additional_rule_2: (As)->As
            omega.pop(0)
            continue
        if stack[-1] in ['(']:
            if len(omega)>0 and omega[0] in ['SCTPAD']: # cell 14
                _error(6, k)
            elif stack in [')', ',']: # cell 15
                _error(7, k)
            else: # cell 13
                shift()
                continue
        if (len(stack)>=2) and (stack[-2] in ['(']) and (stack[-1] in ['MA']): 
            if len(omega)>0 and omega[0] in [',', ')']: # cell 17
                shift()
                continue
            else: # cell 16
                _error(8, k)
        if (len(stack)>=3) and (stack[-3] in ['(']) and (stack[-2] in ['MA']) and (stack[-1] in [')']): # cell 18
            if len(stack)>=4 and stack[-4] in [',']:
                stack.pop()
                stack.pop()
                stack[-1]='A'
            stack.pop()
            stack.pop()
            stack[-1]='As'
            continue
        if stack[-1] in ['A']: # cell 19
            if not( len(stack)>1 and stack[-2] in [','] ):
                stack[-1]='MA'
                continue
        if stack[-1] in ['MA']: # cell 21
            if len(omega)>0 and omega[0] in [',']:
                shift()
                continue
            else: # cell 20
                _error(9,k)
        if (len(stack)>=2) and (stack[-2] in ['MA']) and (stack[-1] in [',']):
            if len(omega)>0 and omega[0] in _signs_in_grammar: # cell 24
                _error(11, k)
            elif len(omega)>0 and omega[0] in sctpad: # cell 23
                _error(10, k)
            else: # cell 22
                if len(omega)>0: shift()
                continue
        if (len(stack)>=3) and (stack[-3] in ['MA']) and (stack[-2] in [',']) and (stack[-1] in ['A']):
            stack.pop() # cell 25
            stack.pop()
            stack[-1]='MA'
            continue
        if stack[-1] in ['I', 'OP', 'Str']: # cell 27
            stack[-1]='S'
            continue
        if stack[-1] in ['N']:  # cell 28
            stack[-1]='('
            stack.append('MA') # mind this feature!!!!
            stack.append(')')
            continue
        if stack[-1] in ['Z']: # cell 26
            if len(stack)==1 and len(omega)==0:
                checkbox[k]=1
                break
            elif len(stack)==1: # if len(omega)>0 but Z is already here in stack
                _error(12, k)
            elif len(stack)>1 and (stack[-2] in ['(']): # if stack is not Z-only --
                stack[-1]='A' # -- (Z -> (A
                continue
            else: # unknown error
                _error(12, k)
        else:
            print('help!!') # if nothing else is gonna work
            #exit()
            _error(0, k)
            break
input() # to leave black window active in Windows-mode
            


    
            
