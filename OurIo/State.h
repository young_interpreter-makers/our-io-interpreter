#pragma once
#include "resource.h"

// ����� ��������� ��������
class State
{
private:
	// ��� ���������
	string name;
	// ������ ����������� ���������
	list<string> categories;
	// ������ ��������� ���������
	list<string> next;
	// ������ ��������� �������� ��� ���������
	list<list<string>> action;

public:
	// ����������� �� ���������
	State() {
		this->name = "UnknownState";
		this->categories;
		this->next;
		this->action;
	}

	// �����������
	State(string name) {
		this->name = name;
		this->categories;
		this->next;
		this->action;
	}

	// ������� ���
	string get_name() {
		return this->name;
	}

	// ������� ��������� ���������
	string get_next(string categories) {
		// c������� �������� �� ������ ������� ������
		auto it_categories = this->categories.begin();
		auto it_next = this->next.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_categories != this->categories.end()) {
			if (*it_categories == categories) {
				return *it_next;
			}
			++it_categories;
			++it_next;
		}
		return "";
	}

	// ������� ���������� �������� ��� ��������
	int get_action_size(string categories) {
		// c������� �������� �� ������ ������� ������
		auto it_categories = this->categories.begin();
		auto it_action = this->action.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_categories != this->categories.end()) {
			if (*it_categories == categories) {
				return it_action->size();
			}
			++it_categories;
			++it_action;
		}
		return 0;
	}

	// �������� �������� � ������ �������
	string get_action(string categories, int n) {
		// c������� �������� �� ������ ������� ������
		auto it_categories = this->categories.begin();
		auto it_action = this->action.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_categories != this->categories.end()) {
			if (*it_categories == categories) {
				// c������� �������� �� ������ ������� ������
				auto it = it_action->begin();
				int i = 0;
				// ���� �������� �� ����� �� ������� ������
				while (it != it_action->end()) {
					if (i == n)
						return *it;
					++it;
					++i;
				}
			}
			++it_categories;
			++it_action;
		}
		return "";
	}

	// �������� ��������� � ��������� ���������
	void add(string categories, string next) {
		this->categories.push_back(categories);
		this->next.push_back(next);
		list<string> temp;
		this->action.push_back(temp);
	}

	// �������� ��������
	void add_action(string categories, string action) {
		// c������� �������� �� ������ ������� ������
		auto it_categories = this->categories.begin();
		auto it_action = this->action.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_categories != this->categories.end()) {
			if (*it_categories == categories) {
				it_action->push_back(action);
			}
			++it_categories;
			++it_action;
		}
	}

	// ������ ���������
	void print()
	{
		// c������� �������� �� ������ ������� ������
		auto it_categories = this->categories.begin();
		auto it_next = this->next.begin();
		auto it_action = this->action.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_categories != this->categories.end()) {
			printf("%s %s %s", name.c_str(), it_categories->c_str(), it_next->c_str());
			// c������� �������� �� ������ ������� ������
			auto it = it_action->begin();
			// ���� �������� �� ����� �� ������� ������
			while (it != it_action->end()) {
				printf(" %s", it->c_str());
				++it;
			}
			printf("\n");
			++it_categories;
			++it_next;
			++it_action;
		}
	}

	// ������ ��������� � ����
	void fprint(FILE* file)
	{
		// c������� �������� �� ������ ������� ������
		auto it_categories = this->categories.begin();
		auto it_next = this->next.begin();
		auto it_action = this->action.begin();
		// ���� �������� �� ����� �� ������� ������
		while (it_categories != this->categories.end()) {
			fprintf(file, "%s %s %s", name.c_str(), it_categories->c_str(), it_next->c_str());
			// c������� �������� �� ������ ������� ������
			auto it = it_action->begin();
			// ���� �������� �� ����� �� ������� ������
			while (it != it_action->end()) {
				fprintf(file, " %s", it->c_str());
				++it;
			}
			fprintf(file, "\n");
			++it_categories;
			++it_next;
			++it_action;
		}
	}
};

