﻿// Io interpreter v1.0.0 - alpha
// 7103
// Golofaev M A
// Rykhlov V V
// Shtepen A V
// 21 dec 2020

#include "resource.h"
#include "Scaner.h"

int main()
{
	//char buff[BUFFSIZE];
	//memset(buff, 0, BUFFSIZE);
	//printf("Enter file name: ");
	//scanf_s("%s", buff, BUFFSIZE);
	//strcpy_s(buff, BUFFSIZE, "prog.txt");
	//char* filename;
	// Объявление имени файла
	//filename = (char*)malloc(BUFFSIZE);
	//strncpy_s(filename, BUFFSIZE, "scaner.txt", 10);

	// Открытие файла
	FILE* IN_PUT;
	fopen_s(&IN_PUT, "_filename", "r");
	if (IN_PUT == NULL)
	{
		printf("Lexeme error(1) - File not found!\n");
		exit(1);
	}

	// Объявление буфферов
	char* buff;
	buff = (char*)malloc(BUFFSIZE * sizeof(char));
	fgets(buff, BUFFSIZE, IN_PUT);

	// Создадим сканер
	Scaner s;
	// Загрузим состояния автомата
	s.read_state();
	// Просканируем файл
	s.scan_file(buff);
	// Получим таблицу имен 
	NameTable table = s.get_table();
	table.print();

	FILE* OUT_PUT;
	fopen_s(&OUT_PUT, "_done", "w");
	_fcloseall();
	return 0;
}
