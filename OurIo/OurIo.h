#pragma once

#include "State.h"

#ifdef PROJECT_EXPORTS
#define PROJECT_API __declspec(dllexport)
#else
#define PROJECT_API __declspec(dllimport)
#endif

extern "C" {

	PROJECT_API void scan(char* filename);

}